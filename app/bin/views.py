from . import app

@app.route("/")
@app.route("/index")
def index():
    '''views for index query'''
    return "Hello world"
