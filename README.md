# elasticsearch-auth

Elasticsearch authentication with flask and nginx

# Работа со стендом

1) Сформировать новый образ контейнера из dockerfile. Новый образ docker файла формируется следующей командой: `docker build . -t myflask:latest` (`make build`)
2) Проверить, что новый образ контейнера успешно добавлен в локальный репозиторий образов docker: `docker images`
3) Внести изменения в docker-compose файл перед запуском стенда. В docker-compose nhебует изменить секцию `image` и пути в секции `volumes`
4) Запустить стенд: `docker-compose up -d` (`make start`)

