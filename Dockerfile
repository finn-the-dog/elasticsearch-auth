FROM frolvlad/alpine-python3:latest

# docker build -t myflask:latest .



# RUN rm -rf /etc/yum.repos.d/*;\
# echo -e "[nailgun]\nname=Nailgun Local Repo\nbaseurl=http://$(route -n | awk '/^0.0.0.0/ { print $2 }'):_PORT_/os/x86_64/\ngpgcheck=0" >
# /etc/yum.repos.d/nailgun.repo;\
# yum clean all;\
# yum --quiet install -y ruby21-nailgun-mcagents sysstat



COPY ./app /app
WORKDIR /app
ENV PYTHONPATH=/app


RUN apk add --no-cache \
        uwsgi-python3

RUN python3.6 -m pip install --upgrade pip \
    && python3.6 -m pip install --no-cache-dir -r requirements.txt \
    && chmod +x /app/run.sh

    # && python3.6 -m pip install uwsgi \

# # prom mode
CMD ["/app/run.sh"]

# # debug mode
# CMD ["/bin/sh"]
